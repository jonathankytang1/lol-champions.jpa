package com.training.lolchampion.controller;

import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.repository.LolChampionRepository;
import com.training.lolchampion.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/lolchampion")
public class LolChampionController {
    @Autowired
    private LolChampionService lolChampion;

    @GetMapping
    public List<LolChampion> findall() {
        return lolChampion.findAll();
    }
}
